/**
* Created with 305 challenge Quote Me - I said It.
* User: bluechiptony
* Date: 2014-12-06
* Time: 10:47 PM
* To change this template use Tools | Templates.
*/

//initialize angular
        var myApp = angular.module('quoteMe', []);
        
        //set up the app controller
        myApp.controller('quoter', function($scope, $http){
            //module goes in here
            
            var urlResource = 'https://crackling-inferno-1732.firebaseio.com/quotes/.json'
            $scope.people = [
               
            ];
            //call the http get function and on success run anonymous functin
            //
            $scope.searchable = [];
            
            $scope.reloadData = function(){
                $http.get(urlResource).success(function(data){
                    $scope.people = data

                    //map the JSOn object ton an array

                     var dataArray = Object.keys(data).map(function(k){
                         return data[k];
                     });
                    //console.log($scope.quotes)
                    //
                    

                });
                
                
            }
            
            $scope.loadForAuthor = function(authorName){
                console.log(authorName)
                
               urlResource = 'https://crackling-inferno-1732.firebaseio.com/quotes/.json';
                
                $http.get(urlResource, $scope.quote).success(function(data, status, headers, config){
                    
                    
                  $http.get(urlResource).success(function(data){
                        $scope.people = data

                        //map the JSOn object ton an array

                         var dataArray = Object.keys(data).map(function(k){
                             return data[k];
                         });
                        //console.log($scope.quotes)
                        //
                        var newAr =[]
                    for (var i = 0; i<dataArray.length; i++){
                        if (dataArray[i].name === authorName){
                            newAr.push(dataArray[i])
                        }
                        
                    }
                    $scope.people = newAr;
                       
                    });
              
                    
                    
                   // alert("Quote and author added");
                   
                }).error(function(data, status, headers, config){
                    alert("UNsucessful");
                    console.log(headers)
                    console.log(config)
                });
                
            }
            
            
            $http.get(urlResource).success(function(data){
                $scope.people = data
                
                //map the JSOn object ton an array
                
                 var dataArray = Object.keys(data).map(function(k){
                     return data[k];
                 });
                //console.log($scope.quotes)
                $scope.people = dataArray;
                $scope.searchable = dataArray
            });
            
            
            
            $scope.addAuthor = function(){
                //assigned value again cos sometimes the url might change and i like the closure thing
                urlResource = 'https://crackling-inferno-1732.firebaseio.com/people/.json'
                
                $http.post(urlResource, $scope.quote).success(function(data, status, headers, config){
                    alert("Quote and author added");
                    $scope.reloadData();
                }).error(function(data, status, headers, config){
                    alert("UNsucessful");
                });
                
            }
            
            $scope.quotes = [
               // {'author': 'Muhammad Ali', 'quote' : 'Float Like a butterfly, Sting like a bee.' }
            ];
            
        });