/**
* Created with 305 challenge Quote Me - I said It.
* User: bluechiptony
* Date: 2014-12-06
* Time: 10:47 PM
* To change this template use Tools | Templates.
*/

//initialize angular
        var myApp = angular.module('quoteMe', []);
        
        //set up the app controller
        myApp.controller('quoter', function($scope, $http){
            //module goes in here
            
            var urlResource = 'https://crackling-inferno-1732.firebaseio.com/quotes/.json'
         
            //call the http get function and on success run anonymous functin
            //u
            $scope.searchable = [];
            $scope.reloadData = function(){
                $http.get(urlResource).success(function(data){
                    $scope.quotes = data

                    //map the JSOn object ton an array

                     var dataArray = Object.keys(data).map(function(k){
                         return data[k];
                     });
                    //console.log($scope.quotes)
                    $scope.quotes = dataArray;

                });
                
                
            }
            
            $scope.loadForAuthor = function(authorName){
                console.log(authorName)
                
               
                
                $http.get(urlResource, $scope.quote).success(function(data, status, headers, config){
                     $http.get(urlResource).success(function(data){
                        $scope.quotes = data

                        //map the JSOn object ton an array

                         var dataArray = Object.keys(data).map(function(k){
                             return data[k];
                         });
                        //console.log($scope.quotes)
                        //
                        var newAr =[]
                    for (var i = 0; i<dataArray.length; i++){
                        if (dataArray[i].Author === authorName){
                            newAr.push(dataArray[i])
                        }
                        
                    }
                    $scope.quotes = newAr;
                       
                    });
              
                    
                   
                }).error(function(data, status, headers, config){
                    alert("UNsucessful");
                    console.log(headers)
                    console.log(config)
                });
            }
            
            
            
            $http.get(urlResource).success(function(data){
                $scope.quotes = data
                
                //map the JSOn object ton an array
                
                 var dataArray = Object.keys(data).map(function(k){
                     return data[k];
                 });
                //console.log($scope.quotes)
                $scope.quotes = dataArray;
                $scope.searchable = dataArray
               
                 
                var newDistinct = dataArray
                var temp = dataArray
                
               Array.prototype.distinct = function() {
                    var derivedArray = [];
                    for (var i = 0; i < this.length; i += 1) {
                        if (!derivedArray.contains(this[i])) {
                            derivedArray.push(this[i])
                        }
                    }
                    return derivedArray;
                };
                //$scope.searchable = dataArray.unique
               
                
                /*
                
                for(var i = 0; i<newDistinct.length; i++){
                    var retAuth = newDistinct[i]
                    for (var j = 0; j < temp.length; j++){
                        if(retAuth.Author === temp[j].Author){
                            temp.pop(temp[j])
                        }   
                    }
                }
                */
                console.log(temp)
                $scope.searchable = dataArray.distinct()
                
              
                
                
                
                
            });
            
            
            
            $scope.addAuthor = function(){
                //assigned value again cos sometimes the url might change and i like the closure thing
                urlResource = 'https://crackling-inferno-1732.firebaseio.com/quotes/.json'
                
                $http.post(urlResource, $scope.quote).success(function(data, status, headers, config){
                    alert("Quote and author added");
                    $scope.reloadData();
                }).error(function(data, status, headers, config){
                    alert("UNsucessful");
                });
                
            }
            
            $scope.quotes = [
               // {'author': 'Muhammad Ali', 'quote' : 'Float Like a butterfly, Sting like a bee.' }
            ];
            
        });